import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import { TypographyComponent } from './typography/typography.component';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { ReactiveFormsModule } from '@angular/forms';
import {MatBadgeModule} from '@angular/material/badge';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import { BottomsheetExComponent } from './autocomplete/bottomsheet-ex/bottomsheet-ex.component';
import {MatListModule} from '@angular/material/list';
import { ExpansionPanelComponent } from './expansion-panel/expansion-panel.component'
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { StepperComponent } from './stepper/stepper.component';
import {MatStepperModule} from '@angular/material/stepper';
import { TabsComponent } from './tabs/tabs.component';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import { ToolbarComponent } from './toolbar/toolbar.component';
import {MatDividerModule} from '@angular/material/divider';
import { NavigationComponent } from './navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatSidenavModule } from '@angular/material/sidenav';
import { FormComponent } from './form/form.component';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';

const MaterialComponent=[
  MatButtonModule,
  MatAutocompleteModule,
  ReactiveFormsModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatListModule,
  MatExpansionModule,
  MatIconModule,
  MatFormFieldModule,
  MatDatepickerModule,
  MatNativeDateModule ,
  MatInputModule,
  MatStepperModule,
  MatTabsModule,
  MatToolbarModule,
  MatDividerModule
]



@NgModule({
  declarations: [
    TypographyComponent,
    AutocompleteComponent,
    BottomsheetExComponent,
    ExpansionPanelComponent,
    StepperComponent,
    TabsComponent,
    ToolbarComponent,
    NavigationComponent,
    FormComponent,
  ],
  imports: [
    CommonModule,
    MaterialComponent,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    ReactiveFormsModule

  ],
  exports:[MaterialComponent,TypographyComponent,AutocompleteComponent,
    ExpansionPanelComponent,StepperComponent,TabsComponent,
    ToolbarComponent,NavigationComponent,
    FormComponent]
})
export class MeterialModule { }
