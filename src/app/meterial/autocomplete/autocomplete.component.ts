import { BottomsheetExComponent } from './bottomsheet-ex/bottomsheet-ex.component';
import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { map, Observable, startWith } from 'rxjs';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css']
})
export class AutocompleteComponent {
  constructor(private _bottomSheet: MatBottomSheet) {}


  myControl = new FormControl('');
  options: string[] = ['One', 'Two', 'Three'];
  filteredOptions!: Observable<string[]>;

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value || '')),
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }


  // badge
  hidden = false;

  toggleBadgeVisibility() {
    this.hidden = !this.hidden;
  }


  // bottom sheet overview
  openBottomSheet(): void {
    this._bottomSheet.open(BottomsheetExComponent);
  }
}
