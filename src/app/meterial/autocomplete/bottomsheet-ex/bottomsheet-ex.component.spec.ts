import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BottomsheetExComponent } from './bottomsheet-ex.component';

describe('BottomsheetExComponent', () => {
  let component: BottomsheetExComponent;
  let fixture: ComponentFixture<BottomsheetExComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BottomsheetExComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BottomsheetExComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
