import { Component } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-bottomsheet-ex',
  templateUrl: './bottomsheet-ex.component.html',
  styleUrls: ['./bottomsheet-ex.component.css']
})
export class  BottomsheetExComponent {
  constructor(private _bottomSheetRef: MatBottomSheetRef<BottomsheetExComponent>) {}

  openLink(event: MouseEvent): void {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }
}
